\contentsline {section}{LIST OF TABLES}{iv}{section*.9}
\contentsline {section}{LIST OF FIGURES}{v}{section*.10}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Document Purpose}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Document Conventions}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Intended Audience and Reading Suggestions}{2}{section.1.3}
\contentsline {section}{\numberline {1.4}Project Scope}{2}{section.1.4}
\contentsline {section}{\numberline {1.5}References}{3}{section.1.5}
\contentsline {chapter}{\numberline {2}System Analysis }{4}{chapter.2}
\contentsline {section}{\numberline {2.1}System Perspective}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Analysis Methodology}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}System Motivation Scenarios}{5}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1} System core functionalities}{5}{subsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.1.1}Feature Acquisition}{5}{subsubsection.2.3.1.1}
\contentsline {subsubsection}{\numberline {2.3.1.2}Patient Temperature Acquisition via Thermometer}{6}{subsubsection.2.3.1.2}
\contentsline {subsubsection}{\numberline {2.3.1.3}Outcome Mediation}{7}{subsubsection.2.3.1.3}
\contentsline {subsubsection}{\numberline {2.3.1.4}Treatment Recommendation}{9}{subsubsection.2.3.1.4}
\contentsline {subsubsection}{\numberline {2.3.1.5}Patient Archive Recording}{9}{subsubsection.2.3.1.5}
\contentsline {subsection}{\numberline {2.3.2}System Management Functionalities}{10}{subsection.2.3.2}
\contentsline {subsubsection}{\numberline {2.3.2.1}User Interaction Management}{10}{subsubsection.2.3.2.1}
\contentsline {subsubsection}{\numberline {2.3.2.2}Internal Communication Management}{11}{subsubsection.2.3.2.2}
\contentsline {subsubsection}{\numberline {2.3.2.3}External Measurement devices Communication Management}{12}{subsubsection.2.3.2.3}
\contentsline {subsubsection}{\numberline {2.3.2.4}Bluetooth Communication Management}{13}{subsubsection.2.3.2.4}
\contentsline {chapter}{\numberline {3}System Modelling}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}System Goal Models }{15}{section.3.1}
\contentsline {section}{\numberline {3.2}High Level Goals and Overall Goal Model }{15}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Sub-goal model for diagnosing patients}{17}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Sub-goal model for identifying patient health problems and recommending treatments}{17}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Sub-goal model for managing patient data}{18}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Sub-goal model for the mediator (internal communication)}{18}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Sub-goal model for external measurement devices}{19}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Sub-goal model for the user interface interaction}{20}{subsection.3.2.6}
\contentsline {section}{\numberline {3.3}System Role Models }{21}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Feature Inspector}{22}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Outcome Mediator role }{26}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}IMCI Outcome Representative Role}{28}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}" Dengue Fever " Representative Role}{30}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Treatment Recommender Role}{32}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Patient Archive Recorder}{34}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Mediator}{35}{subsection.3.3.7}
\contentsline {subsection}{\numberline {3.3.8}Measurement Device Manager}{35}{subsection.3.3.8}
\contentsline {subsection}{\numberline {3.3.9}User Interface Manager}{36}{subsection.3.3.9}
\contentsline {subsection}{\numberline {3.3.10}Bluetooth Manager}{39}{subsection.3.3.10}
\contentsline {subsection}{\numberline {3.3.11}Patient information Manager}{41}{subsection.3.3.11}
\contentsline {chapter}{\numberline {4}System Additional Requirements}{42}{chapter.4}
\contentsline {chapter}{\numberline {5}Conclusion}{43}{chapter.5}
\contentsline {chapter}{\numberline {A}Glossary}{44}{appendix.A}
\contentsline {chapter}{\numberline {B}Project Scope Statement }{45}{appendix.B}
\contentsline {chapter}{\numberline {C}Requirement Change Control Plan}{55}{appendix.C}
